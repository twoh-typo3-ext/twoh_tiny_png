<?php

$EM_CONF['twoh_tiny_png'] = [
    'title' => 'Tiny PNG',
    'description' => 'Optimze your TYPO3 Images with Tiny PNG',
    'category' => 'plugin',
    'author' => 'Andreas Reichel',
    'author_email' => 'a.reichel91@outlook.com',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4.99',
            'php' => '8.0-8.3'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
